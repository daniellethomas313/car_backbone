var singleCar = require('./../models/carModel');
// Namespace our flowerApp
var app = app || {};

// A group (array) of Flower models
app.CarsCollection = Backbone.Collection.extend({

  // What type of models are in this collection?
  model: singleCar,
  comparator: "id"

});

module.exports = app.CarsCollection;