var carView = require('./carView');
//CollectionView has no template of its own
var app = app || {};

app.allCarsView = Backbone.Marionette.CollectionView.extend({

	el: "#cars-container",

	childView: carView,

  	initialize: function() {
  		console.log("All Cars View initialized");
  		this.render();
  	}
});

module.exports = app.allCarsView;