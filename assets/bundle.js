/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var Car = __webpack_require__(2);
	var CarsCollection = __webpack_require__(3);
	var Router = __webpack_require__(4);

	var legacyCar = new Car({
	  id: 0,
	  name: "2016 Subaru Legacy",
	  price: "24,000",
	  lease: "423",
	  finance: "404",
	  img: "http://4.cdn.klamathfallssubaru.inspirelightning.com/wp-content/uploads/2015/12/2016-Subaru-Legacy.png",
	  transmission: "CVT",
	  ext_color: "Crystal Black Silica",
	  int_color: "Black",
	  stock_num: "SU9201XT",
	  mileage: 31844,
	  vin: "JF2SJAKC3FH420009"
	});

	var foresterCar = new Car({
	  id: 1,
	  name: "2015 Subaru Forester 2.5i",
	  price: "26,000",
	  lease: "423",
	  finance: "404",
	  img: "http://www.jdpower.com/sites/default/files/chrome_images/ChromeImageGallery/Expanded/Transparent/640/2015SUB001c_640/2015SUB001c_640_01.png",
	  transmission: "CVT",
	  ext_color: "Crystal Black Silica",
	  int_color: "Black",
	  stock_num: "SU9201XT",
	  mileage: 31844,
	  vin: "4S4BSBCC8G3347407"
	});

	var outbackCar = new Car({
	  id: 2,
	  name: "2014 Subaru Outback",
	  price: "38,000",
	  lease: "423",
	  finance: "404",
	  img: "http://pictures.dealer.com/s/subaruofkeenesne/1014/41b6adc70a0d028a01fe256bfb560cc0.png",
	  transmission: "CVT",
	  ext_color: "Crystal Black Silica",
	  int_color: "Black",
	  stock_num: "SU9201XT",
	  mileage: 31844,
	  vin: "4S4BSBLC3G3322785"
	});

	var cars = new CarsCollection([legacyCar, foresterCar, outbackCar]);

	var app = new Backbone.Marionette.Application({
	  onStart: function(options) {
	    var router = new Router(options);
	    Backbone.history.start();
	  }
	});


	app.start({initialData: cars});



/***/ },
/* 2 */
/***/ function(module, exports) {

	// Namespace our app
	var app = app || {};

	app.singleCar = Backbone.Model.extend({

	  defaults: {
	    exterior_color: "N/A",
	    img: "images/placeholder.jpg",
	    link: "car/details"
	  },
	  urlRoot: "#car/details"
	});


	module.exports = app.singleCar;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var singleCar = __webpack_require__(2);
	// Namespace our flowerApp
	var app = app || {};

	// A group (array) of Flower models
	app.CarsCollection = Backbone.Collection.extend({

	  // What type of models are in this collection?
	  model: singleCar,
	  comparator: "id"

	});

	module.exports = app.CarsCollection;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	var carModel = __webpack_require__(2);
	var allCarsView = __webpack_require__(5);
	var carDetailsView = __webpack_require__(7);
	var app = app || {};

	var ViewManager = {
	    currentView : null,
	    showView : function(view) {
	        if (this.currentView !== null && this.currentView.cid != view.cid) {
	            this.currentView.remove();
	        }
	        this.currentView = view;
	        return view;
	    }
	}
	// Create a Controller, giving it the callbacks for our Router.
	//controller is an object containing the functions that will match the name of the methods defined in the router
	app.CarStoreController = Backbone.Marionette.Object.extend({
		initialize: function() {
	    	this.options.collection = this.getOption('initialData');
	    	 
		},
		index: function() {
			var indexView = new allCarsView({ collection: this.getOption('collection')});
			this.view = ViewManager.showView(indexView);
			console.log(this.getOption('collection'));
		},
		detailsView: function(id) {
			var book = this.options.collection.models[id];
			var detailsView = new carDetailsView({ model: book});
			this.view = ViewManager.showView(detailsView);
		}
	});


	// Pass it into the Router
	app.CarStoreRouter = Backbone.Marionette.AppRouter.extend({
		appRoutes: {
			"": "index",
			"car/details/:id" : "detailsView"
		},
		initialize: function() {
			this.controller = new app.CarStoreController({
				initialData: this.getOption('initialData')
			});
		}
	});

	module.exports = app.CarStoreRouter;




/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	var carView = __webpack_require__(6);
	//CollectionView has no template of its own
	var app = app || {};

	app.allCarsView = Backbone.Marionette.CollectionView.extend({

		el: "#cars-container",

		childView: carView,

	  	initialize: function() {
	  		console.log("All Cars View initialized");
	  		this.render();
	  	}
	});

	module.exports = app.allCarsView;

/***/ },
/* 6 */
/***/ function(module, exports) {

	var app = app || {};


	app.carView = Backbone.Marionette.ItemView.extend({

	  tagName: "div",
	  className: "row car-item",

	  template: "#car-template",

	  events: {
	  	'mouseover': 'addBgColor',
	  	'mouseout': 'removeBgColor',
	    'click': 'goTo'
	  },

	  initialize: _.once(function() {
	    console.log("Single Car View is initialized");
	  }),

	  addBgColor: function() {
	    this.$el.addClass("bgColorImage");
	  },

	  removeBgColor: function() {
	    this.$el.removeClass("bgColorImage");
	  }

	});

	module.exports = app.carView;

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

	var Car = __webpack_require__(2);
	var car = new Car();
	 
	var app = app || {};


	app.CarDetailsView = Backbone.Marionette.ItemView.extend({

		el: "#car-container",

	  	template: "#car-details",

	  	ui: {
	  		button: '.submit'
	  	},

	  	events: {
	  		'click @ui.button': 'saveInfo'
	  	},

	  	initialize: function(id) {
	    	console.log("Car Details View is initialized");
	    	this.render();
	  	},

	  	saveInfo: function() {
	  		console.log("Saved");
	  	}
	});
	module.exports = app.CarDetailsView;

/***/ }
/******/ ]);