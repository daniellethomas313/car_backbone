var Car = require('./models/carModel');
var CarsCollection = require('./collections/carsCollection');
var Router = require('./routes/router');

var legacyCar = new Car({
  id: 0,
  name: "2016 Subaru Legacy",
  price: "24,000",
  lease: "423",
  finance: "404",
  img: "http://4.cdn.klamathfallssubaru.inspirelightning.com/wp-content/uploads/2015/12/2016-Subaru-Legacy.png",
  transmission: "CVT",
  ext_color: "Crystal Black Silica",
  int_color: "Black",
  stock_num: "SU9201XT",
  mileage: 31844,
  vin: "JF2SJAKC3FH420009"
});

var foresterCar = new Car({
  id: 1,
  name: "2015 Subaru Forester 2.5i",
  price: "26,000",
  lease: "423",
  finance: "404",
  img: "http://www.jdpower.com/sites/default/files/chrome_images/ChromeImageGallery/Expanded/Transparent/640/2015SUB001c_640/2015SUB001c_640_01.png",
  transmission: "CVT",
  ext_color: "Crystal Black Silica",
  int_color: "Black",
  stock_num: "SU9201XT",
  mileage: 31844,
  vin: "4S4BSBCC8G3347407"
});

var outbackCar = new Car({
  id: 2,
  name: "2014 Subaru Outback",
  price: "38,000",
  lease: "423",
  finance: "404",
  img: "http://pictures.dealer.com/s/subaruofkeenesne/1014/41b6adc70a0d028a01fe256bfb560cc0.png",
  transmission: "CVT",
  ext_color: "Crystal Black Silica",
  int_color: "Black",
  stock_num: "SU9201XT",
  mileage: 31844,
  vin: "4S4BSBLC3G3322785"
});

var cars = new CarsCollection([legacyCar, foresterCar, outbackCar]);

var app = new Backbone.Marionette.Application({
  onStart: function(options) {
    var router = new Router(options);
    Backbone.history.start();
  }
});


app.start({initialData: cars});

