module.exports = {
  entry: ['./js/app.js'],
  output: {
    path: './assets',
    filename: 'bundle.js'
  }
}