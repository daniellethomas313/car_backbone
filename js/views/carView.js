var app = app || {};


app.carView = Backbone.Marionette.ItemView.extend({

  tagName: "div",
  className: "row car-item",

  template: "#car-template",

  events: {
  	'mouseover': 'addBgColor',
  	'mouseout': 'removeBgColor',
    'click': 'goTo'
  },

  initialize: _.once(function() {
    console.log("Single Car View is initialized");
  }),

  addBgColor: function() {
    this.$el.addClass("bgColorImage");
  },

  removeBgColor: function() {
    this.$el.removeClass("bgColorImage");
  }

});

module.exports = app.carView;