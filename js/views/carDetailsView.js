var Car = require('./../models/carModel');
var car = new Car();
 
var app = app || {};


app.CarDetailsView = Backbone.Marionette.ItemView.extend({

	el: "#car-container",

  	template: "#car-details",

  	ui: {
  		button: '.submit'
  	},

  	events: {
  		'click @ui.button': 'saveInfo'
  	},

  	initialize: function(id) {
    	console.log("Car Details View is initialized");
    	this.render();
  	},

  	saveInfo: function() {
  		console.log("Saved");
  	}
});
module.exports = app.CarDetailsView;