// Namespace our app
var app = app || {};

app.singleCar = Backbone.Model.extend({

  defaults: {
    exterior_color: "N/A",
    img: "images/placeholder.jpg",
    link: "car/details"
  },
  urlRoot: "#car/details"
});


module.exports = app.singleCar;