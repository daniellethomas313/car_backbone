var carModel = require('./../models/carModel');
var allCarsView = require('./../views/allCarsView');
var carDetailsView = require('./../views/carDetailsView');
var app = app || {};

var ViewManager = {
    currentView : null,
    showView : function(view) {
        if (this.currentView !== null && this.currentView.cid != view.cid) {
            this.currentView.remove();
        }
        this.currentView = view;
        return view;
    }
}
// Create a Controller, giving it the callbacks for our Router.
//controller is an object containing the functions that will match the name of the methods defined in the router
app.CarStoreController = Backbone.Marionette.Object.extend({
	initialize: function() {
    	this.options.collection = this.getOption('initialData');
    	 
	},
	index: function() {
		var indexView = new allCarsView({ collection: this.getOption('collection')});
		this.view = ViewManager.showView(indexView);
		console.log(this.getOption('collection'));
	},
	detailsView: function(id) {
		var book = this.options.collection.models[id];
		var detailsView = new carDetailsView({ model: book});
		this.view = ViewManager.showView(detailsView);
	}
});


// Pass it into the Router
app.CarStoreRouter = Backbone.Marionette.AppRouter.extend({
	appRoutes: {
		"": "index",
		"car/details/:id" : "detailsView"
	},
	initialize: function() {
		this.controller = new app.CarStoreController({
			initialData: this.getOption('initialData')
		});
	}
});

module.exports = app.CarStoreRouter;


